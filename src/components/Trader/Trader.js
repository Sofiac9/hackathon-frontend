import { React, useState } from "react";
import './Trader.css';
import axios from 'axios';

const Trader = () => {
    const investmentServiceUrl = "https://hackathon-backend-hackathon-backend.dev5.benivade.com/api/v1/investments";

    // const [investment, setInvestment] = useState({companyName: null,
    //     stockTicker: null,
    //     industry: null,
    //     price: 0,
    //     numOfShares: 0
    // });
    const [companyName, setCompanyName] = useState("");
    const [stockTicker, setStockTicker] = useState("");
    const [industry, setIndustry] = useState("");
    const [price, setPrice] = useState(0);
    const [numOfShares, setNumOfShares] = useState(0);
    
    //const [sellItem, setSellItem] = useState({});


    const handleCompanyNameChange = (event) => {
        setCompanyName({companyName: event.target.value});
    }
    const handleStockTickerChange = (event) => {
        setStockTicker({stockTicker: event.target.value});
    }
    const handleIndustryChange = (event) => {
        setIndustry({industry: event.target.value});
    }
    const handlePriceChange = (event) => {
        setPrice({price: event.target.value});
    }
    const handleSharesChange = (event) => {
        setNumOfShares({numOfShares: event.target.value});
    }

    const postData = async () => {
        // console.log(investment);
        const investmentJSON = {
            companyName: companyName,
            stockTicker: stockTicker,
            industry: industry,
            price: price,
            numOfShares: numOfShares
        }
        axios.post(investmentServiceUrl, investmentJSON)
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    // const handleSellStockTickerChange = (event) => {
    //     setSellItem({stockTicker: event.target.value});
    // }
    // const findByTicker = async () => {
    //     axios.get(byIdInvestmentServiceUrl, {

    //     })
    // }
    // const sellInvestment = async () => {

    // }

    return (
        <div>
            <form class="p-3 mb-2 rounded">
                <div class="row">
                    <div class="col-md-8">
                        <h6>Buy Stock</h6>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn px-2 py-0 btn-dark submit-btn" onClick={postData}>Submit</button>
                    </div>
                </div>
                <div class="form-group col-md-4 pb-1">
                    <label for="stockTicker">Stock Ticker</label>
                    <input type="text" class="form-control form-control-sm" id="stockTicker" placeholder="" onChange={handleStockTickerChange}></input>
                </div>
                <div class="form-group pb-1">
                    <label for="companyName">Company Name</label>
                    <input type="text" class="form-control form-control-sm" id="companyName" placeholder="" onChange={handleCompanyNameChange}></input>
                </div>
                <div class="form-group pb-1">
                    <label for="industry">Industry</label>
                    <input type="text" class="form-control form-control-sm" id="industry" placeholder="" onClick={handleIndustryChange}></input>
                </div>
                <div class="form-row row pb-1">
                    <div class="form-group col-md-4">
                        <label for="price">Price</label>
                        <input type="number" class="form-control form-control-sm" id="price" placeholder="" onClick={handlePriceChange}></input>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="shares">Shares</label>
                        <input type="number" class="form-control form-control-sm" id="shares" placeholder="" onClick={handleSharesChange}></input>
                    </div>
                </div>
            </form>

            <form class="p-3 rounded">
                <div class="row">
                    <div class="col-md-8">
                        <h6>Sell Stock</h6>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn px-2 py-0 btn-dark submit-btn">Submit</button>
                    </div>
                </div>
                <div class="form-row row pb-1">
                    <div class="form-group col-md-4">
                        <label for="stockTicker">Stock Ticker</label>
                        <input type="text" class="form-control form-control-sm" id="stockTicker" placeholder=""></input>
                    </div>
                    <div class="form-group px-0 col-md-4 d-flex ">
                        <button type="button" class="btn btn-light btn-small search-btn px-1 py-0 mt-auto">Find</button>
                    </div>
                </div>
                <div class="form-group pb-1">
                    <label for="companyName">Company Name</label>
                    <input type="text" class="form-control form-control-sm" id="companyName" placeholder=""></input>
                </div>
                <div class="form-group pb-1">
                    <label for="industry">Industry</label>
                    <input type="text" class="form-control form-control-sm" id="industry" placeholder=""></input>
                </div>
                <div class="form-row row pb-1">
                    <div class="form-group col-md-5">
                        <label for="marketValue">Market Value</label>
                        <input type="number" class="form-control form-control-sm" id="marketValue" placeholder=""></input>
                    </div>
                    <div class="form-group col-md-3 px-1">
                        <label for="price">Price</label>
                        <input type="number" class="form-control form-control-sm" id="price" placeholder=""></input>
                    </div>
                    <div class="form-group col-md-4 pl-1">
                        <label for="shares">Shares</label>
                        <input type="number" class="form-control form-control-sm" id="shares" placeholder=""></input>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default Trader;