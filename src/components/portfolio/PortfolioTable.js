import { React, forwardRef, useState, useEffect } from "react";
import MaterialTable from "material-table";
import {
  AddBox,
  ArrowDownward,
  Check,
  ChevronLeft,
  ChevronRight,
  Clear,
  DeleteOutline,
  Edit,
  FilterList,
  FirstPage,
  LastPage,
  Remove,
  SaveAlt,
  Search,
  ViewColumn,
} from "@material-ui/icons";

/*  Component  */
const PortfolioTable = (props) => {
  /*  States  */
  const [data, setData] = useState([]);
  const [isRowClicked, setIsRowClicked] = useState(0);
  // const [currentTicker, setCurrentTicker] = useState("");
  const setCurrentTicker = props.setCurrentTicker;
  /*  Hooks  */
  useEffect(() => {
    // Runs ONCE after initial rendering
    getData();
  }, []);

  async function livePriceData(ticker, numDays) {
    // AWS  Lambda URL
    try {
      const priceServiceUrl =
        "https://jqjfct0r76.execute-api.us-east-1.amazonaws.com/default/PythonPandas";

      if (typeof numDays == "undefined") {
        numDays = 1;
      }
      const response = await fetch(
        priceServiceUrl + "?ticker=" + ticker + "&num_days=" + numDays
      );

      return await response.json();
    } catch (err) {
      console.log("livePriceData Failed!", err);
    }
  }

  const columns = [
    { title: "_id", field: "id", hidden: true },
    { title: "Ticker", field: "stockTicker" },
    { title: "Company", field: "companyName" },
    { title: "Current Price", field: "currentPrice", type: "numeric" },
    { title: "Buy Price", field: "price", type: "numeric" },
    { title: "# of Shares", field: "numOfShares", type: "numeric" },
  ];

  const options = {
    cellStyle: {
      backgroundColor: "#1a2946",
      color: "#FFF",
    },
    headerStyle: {
      backgroundColor: "#4d5c79",
      color: "#FFF",
    },
    searchFieldStyle: {
      color: "#000",
    },
  };

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  //Function to merge data from our api and data from live data lambda
  const getData = async () => {
    try {
      console.log("getData");
      const tempData = [];
      const investmentServiceUrl =
        "https://hackathon-backend-hackathon-backend.dev5.benivade.com/api/v1/investments";

      const investmentResponse = await fetch(investmentServiceUrl);

      const rawData = await investmentResponse.json();

      for (var i = 0; i < rawData.length; i++) {
        const livePriceResponse = await livePriceData(
          rawData[i].stockTicker,
          1
        );
        tempData.push({
          _id: rawData[i].stockTicker,
          stockTicker: rawData[i].stockTicker,
          companyName: rawData[i].companyName,
          currentPrice: livePriceResponse.price_data[0][1].toFixed(2),
          price: rawData[i].price,
          numOfShares: rawData[i].numOfShares,
        });
      }
      setData(tempData);
      console.log("getData", tempData);
    } catch (err) {
      console.log("Get data failed!", err);
    }
  };

  return (
    <div style={{ maxWidth: "100%" }}>
      <MaterialTable
        columns={columns}
        options={options}
        icons={tableIcons}
        data={data}
        title="Portfolio"
        style={{ backgroundColor: "#e8eaee", color: "#000", boxShadow: "none" }}
        onRowClick={(event, rowData) => {
          setIsRowClicked(1);
          setCurrentTicker(rowData.stockTicker, rowData.companyName);
          livePriceData(rowData.stockTicker, 5);
        }}
      />
    </div>
  );
};

export default PortfolioTable;
