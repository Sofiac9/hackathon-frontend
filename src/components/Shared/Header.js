import React from "react";
import "./Header.css";
import logo from "./../../assets/company-logo-light.png";

function Header() {
  return (
    <header id="header" class="header">
      <div class="header-menu row p-2">
        <div class="col"></div>
        <div class="col" align="center">
          <img src={logo} class="logo d-block" alt="logo"></img>
          <h4 class="pt-2">BMS5 Management Co.</h4>
        </div>
        <div class="col"></div>
      </div>
    </header>
  );
}

export default Header;
