import { React, forwardRef, useState, useEffect } from "react";
import {
  ResponsiveContainer,
  AreaChart,
  Area,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
} from "recharts";

const TickerLineChart = (props) => {
  const currentTicker = props.currentTicker;
  const [data, setData] = useState("");

  /*  Hooks  */
  useEffect(() => {
    // Runs ONCE after initial rendering
    getData();
    // console.log(data);
  }, [props.currentTicker]);

  async function livePriceData(ticker, numDays) {
    // AWS  Lambda URL
    const priceServiceUrl =
      "https://jqjfct0r76.execute-api.us-east-1.amazonaws.com/default/PythonPandas";

    if (typeof numDays == "undefined") {
      numDays = 1;
    }
    const response = await fetch(
      priceServiceUrl + "?ticker=" + ticker + "&num_days=" + numDays
    );

    return await response.json();
  }
  const getData = async () => {
    const tempData = await livePriceData(props.currentTicker, 365);
    const lineData = [];
    for (var i = 0; i < tempData.price_data.length; i++) {
      lineData.push({
        date: tempData.price_data[i][0],
        price: tempData.price_data[i][1],
      });
    }
    setData(lineData);
    console.log(lineData);
  };

  return (
    <div>
      {props.currentTicker ? (
        <div>
          <h4>{props.currentTicker}</h4>
          <div style={{ width: "100%", height: 300, padding: 16 }}>
            <ResponsiveContainer width="100%" height="100%">
              <AreaChart
                width={800}
                height={250}
                data={data}
                margin={{ top: 10, right: 30, left: 0, bottom: 0 }}
              >
                <defs>
                  <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor="#213458" stopOpacity={0.8} />
                    <stop offset="95%" stopColor="#213458" stopOpacity={0} />
                  </linearGradient>
                </defs>
                <XAxis dataKey="date" hide="true" />
                <YAxis />
                <CartesianGrid strokeDasharray="3 3" />
                <Tooltip />
                <Legend layout="vertical" />
                <Area
                  type="monotone"
                  dataKey="price"
                  stroke="#213458"
                  fillOpacity={1}
                  fill="url(#colorUv)"
                />
              </AreaChart>
            </ResponsiveContainer>
          </div>
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
};

export default TickerLineChart;
