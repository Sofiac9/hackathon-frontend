import "./App.css";
import "./components/portfolio/PortfolioTable";
import "./components/ticker/TickerLineChart";
import PortfolioTable from "./components/portfolio/PortfolioTable";
import Grid from "@material-ui/core/Grid";
import Header from "./components/Shared/Header";
import Trader from "./components/Trader/Trader";
import TickerLineChart from "./components/ticker/TickerLineChart";
import { React, forwardRef, useState, useEffect } from "react";
import Container from "@material-ui/core/Container";

function App() {
  const [currentTicker, setCurrentTicker] = useState("");

  return (
    <div className="App">
      <Header />
      <Container>
        <div class="p-2">
          <Grid container spacing={2}>
            <Grid item lg={3}>
              <Trader />
            </Grid>
            <Grid item lg={6}>
              <PortfolioTable setCurrentTicker={setCurrentTicker} />
            </Grid>
            <Grid item lg={3}>
              <TickerLineChart currentTicker={currentTicker} />
            </Grid>
          </Grid>
          <footer><p>&copy Copyrights 2021 BMS5 Management Co. </p></footer>
        </div>
      </Container>
    </div>
  );
}

export default App;
